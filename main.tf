resource "aws_instance" "bastioninstance" {
  ami                         = var.ami_id[0]
  instance_type               = "t2.micro"
  subnet_id                   = var.aws_subnet[0]
  associate_public_ip_address = "true"
  key_name                    = "publickey"

  security_groups = ["sg-0cdb2b70d5b06170c"]
  tags = {
    Name  = "terraform_bastion_host"
    name  = "terrafrom_instance"
    owner = "created_by_nitish"
  }
}


resource "aws_instance" "priv-instance" {
  ami                         = var.ami_id[0]
  instance_type               = "t2.micro"
  subnet_id                   = var.aws_subnet_private[0]
  associate_public_ip_address = "false"
  key_name                    = "privateeeeeeeee"

  security_groups = ["sg-0cef881e2f3b68bc0"]
  tags = {
    Name  = "terraform_private_instance1"
    name  = "terrafrom_instance"
    owner = "created_by_nitish"
  }
}
