variable "ami_id" {
  type    = list(any)
  default = ["ami-00399ec92321828f5"]

}

variable "aws_subnet" {
  type    = list(any)
  default = ["subnet-09717cfd70e2ff21d", "subnet-0dfad1182eea39f29"]
}

variable "aws_subnet_private" {
  type    = list(any)
  default = ["subnet-023dd8b60e1140e9b", "subnet-0a402b48c186caf7d"]
}

